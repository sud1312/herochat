import { Component } from '@angular/core';
import { SidebarModule } from 'ng-sidebar';

@Component({
selector: 'app-bar',
styleUrls: [ './app-bar.component.css' ],
templateUrl: './app-bar.component.html'
})

export class AppBarComponent {
 private _opened: boolean = false;

  private _toggleSidebar() {
    this._opened = !this._opened;
  }

}
