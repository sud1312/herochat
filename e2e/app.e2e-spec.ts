import { PrivateChatAppV3Page } from './app.po';

describe('private-chat-app-v3 App', function() {
  let page: PrivateChatAppV3Page;

  beforeEach(() => {
    page = new PrivateChatAppV3Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
